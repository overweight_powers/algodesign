package company_prep.capital;

import java.util.HashSet;

public class QuestionMarks {

    public static void main (String[] args){
        QuestionMarks s = new QuestionMarks();
        String test = "5???5";
        String test2 = "6???5";
        String test3 = "5??5";
        String test4 = "5???54???6ferf5?ferfre?ferf?5";

        System.out.println("test: " + s.isQuestionValid(test) + " should be true");
        System.out.println("test2: " + s.isQuestionValid(test2) + " should be false");
        System.out.println("test3: " + s.isQuestionValid(test3) + " should be false");
        System.out.println("test4: " + s.isQuestionValid(test4) + " should be true");
    }

    //Return true if every pair of numbers that adds up to 10, has 3 question marks
    private boolean isQuestionValid(String s){

        HashSet<Integer> endPairs = new HashSet<>();

      s = s.chars()
                       .filter(c -> Character.isDigit(c) || c == '?')
                               .collect(StringBuilder::new,
                                       StringBuilder::appendCodePoint,
                                       StringBuilder::append
                               ).toString();

        System.out.println("Filtered string:" + s);
        for(int i = 0; i < s.length(); i++){
            if(Character.isDigit(s.charAt(i)) && !endPairs.contains(i)) {
                int countQuestions = 0;
                for (int x = i; x < s.length(); x++) {
                     if(s.charAt(x) == '?'){
                         countQuestions++;
                     }
                     else if(!endPairs.contains(x) &&
                             Integer.valueOf(s.charAt(i)) + Integer.valueOf(s.charAt(x)) == 10
                    ){
                         endPairs.add(x);
                             if(countQuestions >= 3){
                                 return true;
                             }
                             else { return false; }
                    }
                }
            }
        }
        return true;
    }

    //Return true if every pair of numbers adds up to 10, AND between every pair exist 3 question marks
    private boolean isQuestionValid2(String s){
        return true;
    }
}
