package company_prep.capital;

public enum ReleaseTestCadenceV2 {
    BIWEEKLY  (24, 2000.0,  6000.0),
    MONTHLY   (11, 2500.0,  8000.0),
    BIMONTHLY ( 6,    0.0, 12000.0),
    QUARTERLY ( 4,    0.0, 22000.0);

    private int    releases;
    private double simpleCost;
    private double rigorousCost;

    /**
     * Constructs each unique release cadence enum with three attributes
     * @param releases     The number of releases for this enum type for the year
     * @param costSimple   The cost of conducting simple testing for a release
     * @param costRigorous The cost of conducting rigorous testing for a release
     */
    ReleaseTestCadenceV2(int releases, double costSimple, double costRigorous) {
        this.releases = releases;
        this.simpleCost = costSimple;
        this.rigorousCost = costRigorous;
    }

    /**
     * Computes the minimum number of simple tests needed for this release cadence
     * @return the minimum number of simple tests needed for this release cadence
     */
    public int simpleNeeded() {
        if (releases <= 6) return 0;
        if (releases > 12) return (releases / 3) * 2;
        return releases / 2;
    }

    /**
     * Computes the minimum number of rigorous tests needed for this release cadence
     * @return the minimum number of rigorous tests needed for this release cadence
     */
    public int rigorousNeeded() {
        if (releases <= 6) return releases;
        if (releases > 12) return (releases / 3);
        return (int)Math.ceil(releases / 2.0);
    }

    /**
     * For the release cadence determines the total cost to run the minimum required tests.
     * If there is surplus budget the surplus is reallocated to determine the new number of
     * simple and rigorous tests that can be performed, prints this out, and returns the amount
     * of budget remaining after reallocation.
     * @param budget The budget to allocate
     * @return The surplus budget after full allocation.  Will be >= 0 and <= min plan cost.
     */
    public double allocateTestBudget(double budget) {
        int simpleNeeded   = this.simpleNeeded();
        int rigorousNeeded = this.rigorousNeeded();
        double cost = simpleNeeded * simpleCost + rigorousNeeded * rigorousCost;
        double surplus = budget - cost;
        if (surplus >= rigorousCost) {
            int upgradable = (int)(surplus / rigorousCost);
            simpleNeeded   -= upgradable;
            rigorousNeeded += upgradable;
            cost = simpleNeeded * simpleCost + rigorousNeeded * rigorousCost;
            surplus = budget - cost;
        }
        System.out.printf("Simple= %3d, Rigorous= %2d, Cost $%9.2f, Surplus %9.2f, %5s\n",
                simpleNeeded, rigorousNeeded, cost, surplus, ((surplus < 0)? "Fails" : "Meets"));
        return surplus;
    }

    // Main program - expects a budget as numeric as argument
    public static void main(String[] args) {
        double lobBudget = 100000.00;
        for (ReleaseTestCadenceV2 rtc : ReleaseTestCadenceV2.values()) {
            System.out.printf("%10s: ", rtc);
            double budgetOverUnder = rtc.allocateTestBudget(lobBudget);
        }
    }
}
