package company_prep.capital.multithread;

import lombok.Data;

@Data
public class Account {

    private String id;
    private int balance;
    private boolean sensitive;

    public Account(String id, int balance, boolean sensitive) {
        this.id = id;
        this.balance = balance;
        this.sensitive = sensitive;
    }
}
