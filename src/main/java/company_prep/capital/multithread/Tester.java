package company_prep.capital.multithread;

import java.util.Arrays;
import java.util.List;

public class Tester {
    public static void main (String[] args){
        Account account1 = new Account("123411", 1, true);
        Account account2 = new Account("123422", 2, false);
        Account account3 = new Account("123433", 3, true);
        Account account4 = new Account("123444", 4, false);
        Account account5 = new Account("123455", 5, true);

        TokenService tokenService = new TokenService();
       List<Account> list = Arrays.asList(account1, account3, account2, account5, account4);
//
//       list.parallelStream().filter(Account::isSensitive)
//               .forEach(tokenService::tokenizeAccount);

       for(Account a : list){
           new Thread(() -> {
               tokenService.tokenizeAccount(a);
           }).start();
       }

        list.sort(( a, b) -> a.getId().compareTo(b.getId()));

        System.out.println(list);
    }

}
