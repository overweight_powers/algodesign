package company_prep.toast;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

@Data
@AllArgsConstructor
class Menu {
    public String name;
    public Collection<MenuGroup> groups;
    public Double price;
}
