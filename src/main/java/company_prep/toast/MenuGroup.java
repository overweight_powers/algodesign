package company_prep.toast;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

@Data
@AllArgsConstructor
class MenuGroup {
    public String name;
    public Collection<MenuGroup> groups;
    public Collection<MenuItem> items;
    public Double price;
}
