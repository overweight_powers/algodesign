package company_prep.toast;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class MenuItem {
    public String name;
    public Double price;
}
