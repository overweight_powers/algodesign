package company_prep.toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Toaster {

    public static void main(String[] args) {

        MenuItem eggs = new MenuItem("eggs", 5.50);
        MenuItem bacon = new MenuItem("bacon", 6.00);
        List<MenuItem> breakfastItems = Arrays.asList(bacon, eggs);
        MenuGroup breakfast = new MenuGroup("Breakfast", new ArrayList<MenuGroup>(), breakfastItems, 10.00);


        MenuItem ketchup = new MenuItem("ketchup", .50);
        MenuItem hotsauce = new MenuItem("hotsauce", .60);
        List<MenuItem> subBreakfast = Arrays.asList(ketchup, hotsauce);
        MenuGroup subBreakFastGroup = new MenuGroup("subBreakfast", new ArrayList<MenuGroup>(), subBreakfast, 11.00);

        MenuItem fork = new MenuItem("fork", .5);
        MenuItem spoon = new MenuItem("spoon", .20);
        List<MenuItem> utensils = Arrays.asList(fork, spoon);
        MenuGroup utensilsGroup = new MenuGroup("utensils", new ArrayList<MenuGroup>(), utensils, .10);

        subBreakFastGroup.getGroups().add(utensilsGroup);

        breakfast.groups.add(subBreakFastGroup);

        MenuItem peunutbutter = new MenuItem("peunutbutter", 5.75);

        MenuItem burgerItem = new MenuItem("burgerItem", 10.00);
        MenuItem hotDogItem = new MenuItem("hotDogItem", 3.00);
        List<MenuItem> lunchItems = Arrays.asList(burgerItem, hotDogItem, peunutbutter);
        MenuGroup lunch = new MenuGroup("lunch",  new ArrayList<MenuGroup>(), lunchItems, 13.00);


        MenuItem pizza = new MenuItem("pizza", 5.00);
        MenuItem fish = new MenuItem("fish", 4.00);
        MenuItem pasta = new MenuItem("pasta", 7.00);
        List<MenuItem> dinnerItems = Arrays.asList(pizza, fish, pasta);
         MenuGroup dinner = new MenuGroup("dinner",  new ArrayList<MenuGroup>(), dinnerItems, 12.00);

        List<MenuGroup> mainGroup = Arrays.asList(breakfast, lunch, dinner);

        Menu mainMenu = new Menu("Overweight Restuarant", mainGroup, 15.00);

        System.out.println(getPriceOfItem(mainMenu, "fork") + " should equal .5");
        fork.setPrice(null);
        System.out.println(getPriceOfItem(mainMenu, "fork") + " should equal .10");
        utensilsGroup.setPrice(null);
        System.out.println(getPriceOfItem(mainMenu, "fork") + " should equal 11.0");
    }

 /*
    The graph as it stands:
                                                    Main Group
              /                                         |                                                                           \
         Breakfast Group [10.00] items: [Eggs[5.50] - Bacon[6.00]]         Lunch Group[13.00] items:[hotdog[3.00], burger[10.00]]                   Dinner Group[pizza, fish, pasta]
          /
         SubBreakFast Group[11.00] Items[ketchup[.5] - hotsauce [.6] ]
         /
        Utensils group [.10] Items:[fork[.5] - spoon [.2]]



GOAL: Create function that takes in a menu, and item name and returns the price of the menu item
      If item does not have a price, then return the price of the closest parent/menugroup that has a price.
*/



    //Code solution here
    private static double getPriceOfItem(Menu menu, String itemName) {
        double result = 0;


        return result;
    }


















    //----------------------------Don't look here if you don't want to see a possible solution---------------------

      /*
      PseudoCode solution:
      1) Check if menu or itemname is null
      2) Create stack for parent item tracking, pushing the menu price
      3) Iterate through menugroups dfs'ing each group and passing the stack return price if not 0

      dfs steps
      1) Check base case
      2) iterate through menugroups and checking each item in that group. If item found return price, or pop stack until parent with price found
      3) dfs through each subgroup of the current grouping
     */

 // A tested working solution
//    private static double getPriceOfItem(Menu menu, String itemName) {
//        double result = 0;
//        if(menu == null || itemName.isBlank()){
//            return 0.0;
//        }
//        Stack<Double> d = new Stack<>();
//        d.push(menu.price);
//        for(MenuGroup group :  menu.getGroups()){
//            result = dfs(group, itemName, d);
//            if(result != 0.0) {
//                return result;
//            }
//        }
//
//        return result;
//    }
//
//    private static Double dfs(MenuGroup group, String itemName, Stack<Double> parentPrices) {
//        //Base case, making sure to pop stack as we hit the end of a menugroup
//        if(group == null || group.getItems() == null || group.getItems().size() == 0){
//            parentPrices.pop();
//            return 0.0;
//        }
//         // Iterate through menu items checking price. If item found return price or pop stack until parent price is found
//            for(MenuItem item : group.getItems()){
//                if(item.getName().equals(itemName)){
//                    if(item.getPrice() != null) {
//                        return item.getPrice();
//                    }
//                    else{
//                        while(!parentPrices.isEmpty()){
//                            Double res = parentPrices.pop();
//                            if(res != null){
//                                return res;
//                            }
//                        }
//                    }
//                }
//            }
//            //DFS through sub menugroupds, pushing price of each menugroup to the stack
//        for(MenuGroup subGroup :  group.getGroups()){
//            parentPrices.push(subGroup.price);
//            double result = dfs(subGroup, itemName, parentPrices);
//            if(result != 0.0) {
//                return result;
//            }
//        }
//        return null;
//    }


}
