package company_prep.twilio;

import java.util.LinkedHashMap;
import java.util.Map;

//Least recently used
//Goal: All operations must be O(1) time and space
public class LRU extends LinkedHashMap<Integer, Integer> {

    private int capacity;

    public LRU (int capacity){
        super(capacity, .75F, true);
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<Integer, Integer> entry){
        if(this.size() > capacity){
            return true;
        }
        return false;
    }

    public void put(int key, int value) {
        super.put(key, value);
    }

    public int get(int key){
        return super.get(key);
    }
}
