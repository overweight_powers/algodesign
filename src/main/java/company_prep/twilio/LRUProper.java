package company_prep.twilio;

import java.util.HashMap;

public class LRUProper {

    public class Node {
        public int key;
        public int value;
        public Node next;
        public Node prev;
    }

    public void addNode(Node node) {
        node.prev = head;
        Node nodeToMove = head.next;
        node.next = nodeToMove;
        nodeToMove.prev = node;
        head.next = node;
    }

    //Remove node from the list
    public void removeNode(Node node) {
        Node prev = node.prev;
        Node next = node.next;
        prev.next = next;
        next.prev = prev;
    }

    public void moveNodeToHead(Node n){
        removeNode(n);
        addNode(n);
    }

    //Remove the LRU node
    public Node popTail() {
        Node res = tail.prev;
        removeNode(res);
        return res;
    }

    private int capacity;
    private HashMap<Integer, Node> store = new HashMap<>();
    private Node head;
    private Node tail;

    public LRUProper(int capacity) {
        this.capacity = capacity;
        head = new Node();
        tail = new Node();
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        Node node = store.get(key);
        if(node ==  null){
            return -1;
        }
        moveNodeToHead(node);

        return node.value;
    }

    public void put(int key, int value) {
        Node node;
        if (store.containsKey(key)) {
            node = store.get(key);
            node.value = value;
            removeNode(node);
        } else {
            node = new Node();
            node.value = value;
            node.key = key;

            if(store.size() >= this.capacity){
                Node t = popTail();
                store.remove(t.key);
            }
        }

        addNode(node);
        store.put(key, node);
    }

    public int getSize() {
        return store.size();
    }
}
