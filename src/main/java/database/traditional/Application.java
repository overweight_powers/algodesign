package database.traditional;

import java.util.ArrayList;
import java.util.Arrays;

public class Application {

    /*
    Design notes:
    Schema is controlled by Table class. You can build the schema, but once a table is created with
    the schema. It is only modifieable through the table class functions.
     */
    public static void main(String[] args){
        Database db = new Database("Dillons DB");
        Schema schema = new Schema();
        schema.addColumn("id", Schema.DATA_TYPE.INTEGER);
        schema.addColumn("first_name", Schema.DATA_TYPE.STRING);
        schema.setPrimaryKey("id");

        Table table = new Table("table", schema);
        Row newRow = table.getNewRow();

        newRow.addColumnData(table.getColumnNames()[0], 10);
        newRow.addColumnData(table.getColumnNames()[1], "Dillon");

        table.createRow(newRow);
        System.out.println("SELECT *;" + table.getRows().toString());
        
        db.addTable(table);
    }
}
