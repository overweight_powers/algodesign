package database.traditional;

import java.util.HashMap;

public class Database {

    private String name;
    private HashMap<String, Table> tables;


    public Database(String name){
        this.name = name;
        this.tables = new HashMap<>();
    }

    public void addTable(Table table){
        tables.put(table.getName(), table);
    }

    public String getName() {
        return name;
    }

}
