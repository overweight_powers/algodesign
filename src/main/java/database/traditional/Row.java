package database.traditional;

import java.util.HashMap;

public class Row {
    private Schema schema;
    private HashMap<String, Object> rowContents = new HashMap<>();

    public Row(Schema schema){
        this.schema = schema;
    }

    public void addColumnData(String colName, Object data){
        System.out.println("Comparing schema column data type: " +  schema.getColumnType(colName)
                + " to actual input data type" + data.getClass().toString());
        if(schema.containsColumnName(colName) && data.getClass().toString().toLowerCase().contains(schema.getColumnType(colName).toString().toLowerCase())){
            rowContents.put(colName, data);
        }
        else{
            throw new RuntimeException("Data type does not match column data type, or column  name does not exist in schema");
        }
    }

    public boolean containsValue(String columnName, Object value){
        return rowContents.containsKey(columnName) && rowContents.get(columnName).equals(value);
    }

    @Override
    public String toString() {
        return "Row{" +
                "schema=" + schema +
                ", rowContents=" + rowContents +
                '}';
    }
}
