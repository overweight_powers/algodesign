package database.traditional;

import java.util.HashMap;

public class Schema {

    public enum DATA_TYPE{
        STRING,
        BOOLEAN,
        INTEGER
    }

    private String primaryKey;
    private HashMap<String, DATA_TYPE> schema;

    public Schema(){
        this.schema = new HashMap<>();
    }

    public void addColumn(String name, DATA_TYPE dataType){
        schema.put(name, dataType);
    }

    public boolean containsColumnName(String name){
        return schema.containsKey(name);
    }

    public DATA_TYPE getColumnType(String name){
        return schema.get(name);
    }

    public void setPrimaryKey(String columnName){
        if(schema.containsKey(columnName)){
            this.primaryKey = columnName;
        }
        else{
            throw new RuntimeException("Column " + columnName + " not found in table. Add column before setting as primary key");
        }
    }

    public String[] getColumnNames(){
        return schema.keySet().toArray(new String[schema.size()]);
    }
}
