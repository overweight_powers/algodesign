package database.traditional;

import java.util.ArrayList;
import java.util.List;

public class Table {
    private String name;
    private Schema schema;
    private ArrayList<Row> rows;

    public Table(String name, Schema schema){
        this.name = name;
        this.schema = schema;
        this.rows = new ArrayList<>();
    }

    public String getName(){
        return this.name;
    }

    public Row getNewRow(){
        return new Row(this.schema);
    }

    public String[] getColumnNames(){
       return schema.getColumnNames();
    }
    
    public void createRow(Row row){
        rows.add(row);
    }

    public List<Row> getRows(){
        return rows;
    }

    public ArrayList<Row> getColumns(String columnName, String columnValue) {
        ArrayList<Row> resRows = new ArrayList<>();
        for(Row row : this.rows){
            if(row.containsValue(columnName, columnValue)){
                resRows.add(row);
            }
        }
        return resRows;
    }
}
