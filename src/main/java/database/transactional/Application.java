package database.transactional;

public class Application {

    public static void main(String[] args){
        TransactionalMap<String, String> map = new TransactionalMap<>();
        map.create("hello", "bob");
        map.delete("hello");
        map.beginTransaction();
        map.create("hello", "bob");
        map.update("hello", "bob2");
        map.delete("hello");
        map.commitTransaction();
    }
}
