package database.transactional;

//Command class represent a SQL Command statement "select xyz"
public class Command <K, V>{
    public enum COMMAND_TYPES{
        SELECT,
        DELETE,
        UPDATE,
        CREATE
    }

    public Command (COMMAND_TYPES comandType, K key, V value){
        this.commandType = comandType;
        this.key = key;
        this.value = value;
    }

    public Command (COMMAND_TYPES comandType, K key){
        this.commandType = comandType;
        this.key = key;
    }

    private COMMAND_TYPES commandType;
    private K key;
    private V value;

    public K getKey(){
        return this.key;
    }
    public V getValue(){
        return this.value;
    }

    public COMMAND_TYPES getCommandType(){
        return this.commandType;
    }
}
