package database.transactional;

import java.util.LinkedList;

//Transaction class represents a list of commands, and an interface for that list
public class Transaction {

    private LinkedList<Command> transactions;

    public Transaction(){
        transactions =  new LinkedList<>();
    }

    public void addCommand(Command command){
        transactions.offerLast(command);
    }

    public Command getCommand(Command command){
        return transactions.pollFirst();
    }

    public LinkedList<Command> getCommands(){
        return transactions;
    }

    public boolean isEmpty(){
        return transactions.isEmpty();
    }

}
