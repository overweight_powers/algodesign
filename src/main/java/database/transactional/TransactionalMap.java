package database.transactional;

import java.util.HashMap;

public class TransactionalMap<K, V> {

    private final HashMap<K, V> map;
    boolean transactionInProgress;
    private Transaction transaction;


    public TransactionalMap() {
        this.map = new HashMap<>();
    }

    public void beginTransaction() {
        transaction = new Transaction();
        transactionInProgress = true;
    }

    public void commitTransaction() {
        this.transactionInProgress = false;
        for (Command<K, V> command : transaction.getCommands()) {
            K key = command.getKey();
            V value = command.getValue();

            switch (command.getCommandType()) {
                case UPDATE:
                    this.update(key, value);
                    break;
                case DELETE:
                    this.delete(key);
                    break;
                case CREATE:
                    this.create(key, value);
                    break;
            }
        }
    }

    public void create(K key, V value) {
        if (transactionInProgress) {
            Command command = new Command(Command.COMMAND_TYPES.CREATE, key, value);
            transaction.addCommand(command);
        } else {
            this.map.put(key, value);
        }
    }

    public void delete(K key) {
        if (transactionInProgress) {
            Command command = new Command(Command.COMMAND_TYPES.DELETE, key, key);
            transaction.addCommand(command);
        } else {
            this.map.remove(key);
        }
    }

    public void update(K key, V value) {
        if (transactionInProgress) {
            Command command = new Command(Command.COMMAND_TYPES.CREATE, key, value);
            transaction.addCommand(command);
        } else {
            this.map.put(key, value);
        }
    }

    public V get(K key) {
        return map.get(key);
    }
}
