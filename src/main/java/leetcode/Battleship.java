package leetcode;

public class Battleship {

    int ROWS;
    int COLS;

    public static void main(String[] args) {
        Battleship battle = new Battleship();

        int[][] grid = new int[][]{
                {0, 1, 1, 1, 0},
                {0, 0, 0, 1, 0},
                {0, 0, 0, 1, 1},
                {1, 1, 0, 0, 0},
                {0, 0, 0, 0, 0}};
        String res = battle.isGridValid(grid) ? "valid" : "invalid";
        System.out.println("this grid is " + res);
    }

    private boolean isGridValid(int[][] board) {
        if (board == null || board.length == 0 || board[0].length == 0) {
            return false;
        }

        this.ROWS = board.length;
        this.COLS = board[0].length;

        for (int r = 0; r < ROWS; r++) {
            for (int c = 0; c < COLS; c++) {
                if (board[r][c] == 1) {
                    if (board[r][c + 1] == 1 && !isShipValid(board, r, c, false)) {
                        return false;
                    } else if (board[r + 1][c] == 1 && !isShipValid(board, r, c, true)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean isShipValid(int[][] board, int r, int c, boolean isVertical) {

        if (r == ROWS || c == COLS || board[r][c] == 0) {
            return true;
        }

        if (!isVertical
                && (r + 1 < ROWS && board[r + 1][c] == 1)) {
            return false;
        } else if (isVertical
                && (c + 1 < COLS && board[r][c + 1] == 1)
                || (c - 1 >= 0 && board[r][c - 1] == 1)) {
            return false;
        }

        board[r][c] = 0;

        r = isVertical ? r + 1 : r;
        c = isVertical ? c : c + 1;
        return isShipValid(board, r, c, isVertical);

    }

}
