package leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class BillDotCom {
    public static void main(String[] args) {
        BillDotCom s = new BillDotCom();
        int maxCapacity = 5;
        int[][] trips = {{1, 3, 4}, {5, 2, 5}};


        /*
        Return true if we never go over capacity in our trips array
        IE. Overlapping trips cannot have capacity greater than max capacity
          index 0 number needing a ride
                1 from time
                2 to time
         */
        System.out.println(s.doTripsWork(trips, maxCapacity));
    }

    private boolean doTripsWork(int[][] trips, int maxCapacity) {
        Arrays.sort(trips, (i, x) -> Integer.compare(i[1], x[1]));

        int capacityOverlap = 0;
        int[] previousTrip = null;
        for (int i = 0; i < trips.length; i++) {
            int currTripCapacity = trips[i][0];
            System.out.println("trips overlap: " + doTwoTripsOverlap(trips[i], previousTrip));
            if (doTwoTripsOverlap(trips[i], previousTrip)) {
                capacityOverlap += currTripCapacity;
            } else{
                capacityOverlap = currTripCapacity;
            }

            if(capacityOverlap > maxCapacity){
                return false;
            }
            previousTrip = trips[i];

        }

        return true;
    }

    private boolean doTwoTripsOverlap(int[] tripA, int[] tripB) {

        if (tripA == null || tripB == null) {
            return false;
        }

        return (tripA[1] <= tripB[2]) && (tripA[2] >= tripB[1]);
    }
}
