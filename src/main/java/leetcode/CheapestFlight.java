package leetcode;

import java.util.*;

public class CheapestFlight {

    //(int destination, int price, int src)
    public static class Flight {
        public int destination;
        public int price;
        public int src;

        public Flight(int destination, int price, int src) {
            this.price = price;
            this.src = src;
            this.destination = destination;
        }
    }

    ;
    private int cheapestPrice = -1;

    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int k) {


        Deque<Flight> dstQueue = new ArrayDeque<>();

        //Build Adjacency List
        HashMap<Integer, List<Flight>> flightMap = new HashMap<>();
        for (int[] flight : flights) {
            int flightSrc = flight[0];
            int flightDst = flight[1];
            int flightPrice = flight[2];
            Flight destination = new Flight(flightDst, flightPrice, flightSrc);

            if (flightSrc == src) {
                dstQueue.offerLast(destination);
            }

            if (flightMap.containsKey(flightSrc)) {
                flightMap.get(flightSrc).add(destination);
            } else {
                List<Flight> destinations = new ArrayList<>();
                destinations.add(destination);
                flightMap.put(flightSrc, destinations);
            }
        }


        int stops = 0;
        int currentPrice = 0;

// {Integer@710} 0 -> {ArrayList@711}  size = 2
//{Integer@712} 1 -> {ArrayList@713}  size = 1
//{Integer@714} 2 -> {ArrayList@715}  size = 1

// [[0,1,1],[0,2,5],[1,2,1],[2,3,1]]
//        int src = 0;
//        int dst = 3;
        while (!dstQueue.isEmpty() & stops <= k) { //initial list of flights with same src
            stops++;
            int size = dstQueue.size();
            System.out.println("here");
            for (int i = 0; i < size; i++) { //go through a single round of 'stops'/flights in the path
                Flight flight = dstQueue.pollFirst();

                if (flight.destination == dst) {
                    cheapestPrice = (cheapestPrice == -1) ? flight.price : Math.min(cheapestPrice, flight.price);
                    continue;
                }
                List<Flight> flightList = flightMap.get(flight.destination);
                for (Flight flightIter : flightList) {
                    Flight flightCopy = new Flight(flightIter.destination, flightIter.price, flightIter.src);
                    if (flightCopy.destination != src) {
                        flightCopy.price = (flight.price + flightIter.price);
                        if(cheapestPrice != -1 && flightCopy.price > cheapestPrice){
                            continue;
                        }
                        dstQueue.offerLast(flightCopy);
                    }

                }

            }

        }


        return cheapestPrice;


    }
}
