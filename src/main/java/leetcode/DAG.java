package leetcode;


import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


/*
Projects have dependencies (that are other projects). For a project to start, all
its dependencies need to be completed. Given a collection of projects (denoted
by characters), and a collection of dependencies (i.e. pairs of <dependent, dependency>),
return a collection of projects with a valid execution order. There might be
multiple valid outputs.

INPUT:
Projects: [a, b, c, d, e, f]
Dependencies: [ [d, a], [b, f], [d, b], [a, f], [c, d] ]
           Note: Dependency pair [d, a] means 'd' depends on 'a' to be completed first.
OUTPUT:
Some examples of valid order:
f e a b d c
f a b d c e
*/
public class DAG {

    public static void main(String[] args) {
//        List<Character> project = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f');
//
//        List<Character> dependency1 = Arrays.asList('d', 'a');
//        List<Character> dependency2 = Arrays.asList('b', 'f');
//        List<Character> dependency3 = Arrays.asList('d', 'b');
//        List<Character> dependency4 = Arrays.asList('a', 'f');
//        List<Character> dependency5 = Arrays.asList('c', 'd');
//
//
//        List<List<Character>> dependencies = new ArrayList<>();
//        dependencies.add(dependency1);
//        dependencies.add(dependency2);
//        dependencies.add(dependency3);
//        dependencies.add(dependency4);
//        dependencies.add(dependency5);
//
//
//        System.out.println(getValidExecutionOrder(project, dependencies));
        int[][] courseList = {{1, 0}};
        System.out.println(courseScheduleII(2, courseList));

    }

    // 0 -> 1
    public static int[] courseScheduleII(int numCourses, int[][] prerequisites) {
        // 1) build adjacency list {key: pre-requisate (b), value: [courses]}
        HashMap<Integer, ArrayList<Integer>> adjMap = new HashMap<>();
        HashMap<Integer, Integer> preReqCount = new HashMap<>();
        for (int[] courses : prerequisites) {
            int course = courses[0];
            int preReq = courses[1];

            ArrayList<Integer> courseList = adjMap.getOrDefault(preReq, new ArrayList<Integer>());
            courseList.add(course);
            adjMap.put(preReq, courseList);

            // 2) build a direction list {key: course, value: count of pre-req(b) courses required}
            preReqCount.put(course, preReqCount.getOrDefault(course, 0) + 1);
        }


        Deque<Integer> q = new ArrayDeque<>();
        for (int i = 0; i < numCourses; i++) {
            if (!preReqCount.containsKey(i)) {
                q.offerLast(i); //adds all the root nodes with no dependencies
            }
        }

        int[] res = new int[numCourses];
        int edgesRemoved = 0;  // keep track of edges removed
        while (!q.isEmpty()) {
            int node = q.pollFirst();
            res[edgesRemoved++] = node;
            if (adjMap.containsKey(node)) {
                for (int course : adjMap.get(node)) {
                    if (preReqCount.containsKey(course)) {
                        preReqCount.put(course, preReqCount.get(course) - 1);
                        if (preReqCount.get(course) == 0) {
                            q.offerLast(course);
                        }
                    }
                }
            }
        }
        return (edgesRemoved == numCourses) ? res : new int[0];
    }

    /*
{
    d : {a, b}
    b:  {c, e }  2
    when select c, observe that b depends on c, and b's count goes to 1
    when select e, observe that b depends on e, and b's count goes to 0  <== when it goes to 0, you know you can select b

    countsMap?
    d: 2
    b: 2
    a: 0
    c: 0
    e: 0

    reverse map
    c: b
    a: d
    b: d
    e: b



    [a, b, c, d, e, f]

}
*/
    private static List<Character> getValidExecutionOrder(List<Character> projects, List<List<Character>> dependencies) {

        List<Character> response = new ArrayList<>();
        HashMap<Character, List<Character>> dependencyMap = new HashMap<>();
        HashMap<Character, Integer> dependencyCount = new HashMap<>();

        if (projects == null || dependencies == null) {
            return response;
        }

        // 1 ) Build dependency hashmap <project, listOfDependencies>

        // d : {a, b}
        //   b:  {c, e }  2
        for (int i = 0; i < dependencies.size(); i++) {
            Character project = dependencies.get(i).get(1);
            Character dependent = dependencies.get(i).get(0);

            if (dependencyMap.containsKey(project)) {
                List<Character> dependencyList = dependencyMap.get(project);
                dependencyList.add(dependent);
            } else {
                List<Character> dependencyList = new ArrayList<>();
                dependencyList.add(dependent);
                dependencyMap.put(project, dependencyList);
            }


            dependencyCount.put(project, dependencyCount.getOrDefault(project, 0) + 1);

        }

        Queue<Character> queue = new LinkedList<>();

        // 2 ) iterate through project list and find a project that doesnt have dependencies
        for (char c : projects) {
            if (!dependencyMap.containsKey(c)) {
                queue.offer(c);
                response.add(c);
                break;
            }
        }

//            //3 ) Iterate through the queue follow the project path
//            while(!queue.isEmpty()){
//                char node = queue.poll();
//                dependencyMap.
//
//            }


        return response;
    }


}
