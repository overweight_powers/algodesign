package leetcode;

import java.util.TreeMap;

public class Indeed {

    /*Q3. Given an array “cleanTitles” consisting of titles such as “Software Engineer”,
     “Mechanical Engineer” and so on. You are then provided with a “rawTitle” for eg,
      “Require a software engineer, paying $10000”.
      You are supposed to find which cleanTitle that has the most overlapping words
       (this is the scoring function) and then return that clean title.
     */



//    public String findTile(String[] tileList, String rawTitle){
//        Map<String, String<>>
//        String[] rawTitleWords = rawTitle.split(" ");
//        for(String word : rawTitleWords){
//            if(titleMap.contains(word)){
//                listOfTitles: titleMap.get(word);
//            }
//        }
//    }

    public int[] topKFrequent(int[] nums, int k) {
        var intCountMap = new TreeMap<Integer, Integer>();
        for(int i : nums){
            intCountMap.put(i, intCountMap.getOrDefault(i, 0) + 1);
        }

        var res = new int[k];
        int counter = 0;
        for(Integer i : intCountMap.keySet()){
            res[counter++] = i;
            if(counter>=k) break;
        }
        return res;
    }


}
