
package leetcode;
/*

Suppose we have an unsorted log file of accesses to web resources. Each log entry consists of an access time, the ID of the user making the access, and the resource ID.

The access time is represented as seconds since 00:00:00, and all times are assumed to be in the same day.

Example:
logs1 = [
    ["58523", "user_1", "resource_1"],
    ["62314", "user_2", "resource_2"],
    ["54001", "user_1", "resource_3"],
    ["200", "user_6", "resource_5"],
    ["215", "user_6", "resource_4"],
    ["54060", "user_2", "resource_3"],
    ["53760", "user_3", "resource_3"],
    ["58522", "user_22", "resource_1"],
    ["53651", "user_5", "resource_3"],
    ["2", "user_6", "resource_1"],
    ["100", "user_6", "resource_6"],
    ["400", "user_7", "resource_2"],
    ["100", "user_8", "resource_6"],
    ["54359", "user_1", "resource_3"],
]

["resourcex, numberOfAccess"]

Example 2:
logs2 = [
    ["300", "user_1", "resource_3"],
    ["599", "user_1", "resource_3"],
    ["900", "user_1", "resource_3"],
    ["1199", "user_1", "resource_3"],
    ["1200", "user_1", "resource_3"],
    ["1201", "user_1", "resource_3"],
    ["1202", "user_1", "resource_3"]
]

Example 3:
logs3 = [
    ["300", "user_10", "resource_5"]
]

Write a function that takes the logs and returns the resource with the highest number of accesses in any 5 minute window, together with how many accesses it saw.

Expected Output:
most_requested_resource(logs1) # => ('resource_3', 3)
Reason: resource_3 is accessed at 53760, 54001, and 54060

most_requested_resource(logs2) # => ('resource_3', 4)
Reason: resource_3 is accessed at 1199, 1200, 1201, and 1202

most_requested_resource(logs3) # => ('resource_5', 1)
Reason: resource_5 is accessed at 300

Complexity analysis variables:

n: number of logs in the input
*/

import java.util.*;

public class IndeedKarat {
    public static void main(String[] argv) {

        String[][] logs1 = new String[][]{
                {"58523", "user_1", "resource_1"},
                {"62314", "user_2", "resource_2"},
                {"54001", "user_1", "resource_3"},
                {"200", "user_6", "resource_5"},
                {"215", "user_6", "resource_4"},
                {"54060", "user_2", "resource_3"},
                {"53760", "user_3", "resource_3"},
                {"58522", "user_22", "resource_1"},
                {"53651", "user_5", "resource_3"},
                {"2", "user_6", "resource_1"},
                {"100", "user_6", "resource_6"},
                {"400", "user_7", "resource_2"},
                {"100", "user_8", "resource_6"},
                {"54359", "user_1", "resource_3"},
        };

        String[][] logs2 = new String[][]{
                {"300", "user_1", "resource_3"},
                {"599", "user_1", "resource_3"},
                {"900", "user_1", "resource_3"},
                {"1199", "user_1", "resource_3"},
                {"1200", "user_1", "resource_3"},
                {"1201", "user_1", "resource_3"},
                {"1202", "user_1", "resource_3"}
        };

        String[][] logs3 = new String[][]{
                {"300", "user_10", "resource_5"}
        };


 IndeedKarat solution = new IndeedKarat();
        String[]  res =  solution.getMostAccessedResource(logs1);
for(String s : res){
    System.out.println(s);
}



    }


    private String[] getMostAccessedResource(String[][] logs) {
        HashMap<String, ArrayList<Integer>> resourceUsageMap = new HashMap<>();

        String[] res = new String[2];
        String maxResource = "";
        int maxAccessed = 0;

        for (int i = 0; i < logs.length; i++) {
            String resourceID = logs[i][2];
            String accessTime = logs[i][0];
            if(resourceUsageMap.containsKey(resourceID)){
                resourceUsageMap.get(resourceID).add(Integer.parseInt(accessTime));
            }
            else{
                ArrayList<Integer> accessTimeList = new ArrayList<Integer>();
                accessTimeList.add(Integer.parseInt(accessTime));
                resourceUsageMap.put(resourceID, accessTimeList);
            }
        }

        for(Map.Entry<String, ArrayList<Integer>> entry : resourceUsageMap.entrySet()){
            ArrayList<Integer> currentTimeList = entry.getValue();
            Collections.sort(currentTimeList);

            for(int i = 0; i<currentTimeList.size(); i++){
                int timeAccessed = currentTimeList.get(i);
                int x = i;
                int count = 0;
                while(x<currentTimeList.size() && (currentTimeList.get(x) <= timeAccessed + 300) ){
                        if(maxAccessed < ++count){
                            maxAccessed = count;
                            maxResource = entry.getKey();
                        }
                        x++;
                }
            }
            System.out.println("Value latest: " + entry.getValue());
        }

        res[0] = maxResource;
        res[1] = String.valueOf(maxAccessed);


        return res;
    }


    private HashMap<String, int[]> getOrganizedLogs(String[][] logs) {

        HashMap<String, int[]> res = new HashMap<>();

        for (int i = 0; i < logs.length; i++) {
            String userID = logs[i][1];
            for (int x = 0; x < logs[i].length; x++) {
                int accessTime = Integer.valueOf(logs[i][0]);

                if (!res.containsKey(userID)) {
                    int[] accessTimeList = new int[2];
                    accessTimeList[0] = accessTime;
                    res.put(userID, accessTimeList);
                } else {
                    int[] accessTimes = res.get(userID);
                    int earliestTime = accessTimes[0];
                    int latestTime = accessTimes[1];
                    if (accessTime > latestTime) {
                        accessTimes[1] = accessTime;
                    } else if (accessTime < earliestTime) {
                        accessTimes[0] = accessTime;
                    }


                }
            }
        }

        return res;

    }

}
