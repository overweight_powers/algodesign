package leetcode;

/*
Let's say you have a list of item objects:

public class Item {
     public String tagOne;
     public String tagTwo;
}
And you are given a single tag as input and a list of items. Return a list of items contains that tag, and all related tags.

So if you get an input tag = "blue"

And your item list is:

[
item1: {tagOne = "blue", tagTwo = "yellow"},
item2: {tagOne = "pink", tagTwo="red"},
item3: {tagOne = "orange", tagTwo="yellow"},
item4: {tagOne = "green", tagTwo="orange"}
]
You would return a list with {item1, item3, item4}. Because Blue has a relationship with yellow, and yellow has a relationship with orange.
 */


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Item {

     public String tagOne;
     public String tagTwo;

     public Item(String tagOne, String tagTwo){
         this.tagOne = tagOne;
         this.tagTwo = tagTwo;
     }

    @Override
    public String toString() {
        return "Item{" +
                "tagOne='" + tagOne + '\'' +
                ", tagTwo='" + tagTwo + '\'' +
                '}';
    }

    public static Set<Item> getAssociatedItemListRecursively(String initialTag, ArrayList<Item> inputList){
        Set<Item> responseList = new HashSet<>();

        for(int x = 0; x<inputList.size(); x++){
            Item item = inputList.get(x);
            if(item.tagOne.equals(initialTag) || item.tagTwo.equals(initialTag)) {
                ArrayList<Item> clone = inputList;
                clone.remove(x);
                responseList.add(item);
                responseList.addAll(getAssociatedItemListRecursively(item.tagOne, clone));
                responseList.addAll(getAssociatedItemListRecursively(item.tagTwo, clone));
            }
        }

        return responseList;
    }
//
//    //failing
//    public static Set<Item> getAssociatedItemListIteratively(String initialTag, ArrayList<Item> inputList){
//        HashSet<Item> responseList = new HashSet<>();
//        for(int x = 0; x<inputList.size(); x++){
//            Item item = inputList.get(x);
//            if(item.tagOne.equals(initialTag) || item.tagTwo.equals(initialTag)) {
//                responseList.add(item);
//                HashSet<Item> responseListItemClone = responseList;
//                while(responseListItemClone.iterator().hasNext()) {
//                    for (int y = 0; y < inputList.size(); y++) {
//                        Item innerItem = inputList.get(y);
//                        if (responseListItem.tagOne.equals(innerItem.tagOne) || responseListItem.tagOne.equals(innerItem.tagTwo) || responseListItem.tagTwo.equals(innerItem.tagOne) || responseListItem.tagTwo.equals(innerItem.tagTwo)) {
//                            responseList.add(innerItem);
//                        }
//                    }
//                }
//            }
//        }
//
//        return responseList;
//    }


}
