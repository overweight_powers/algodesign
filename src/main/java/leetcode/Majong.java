package leetcode;



    /*
While your players are waiting for a game, you've developed a solitaire game for the players to pass the time with.
The player is given an NxM board of tiles from 0 to 9 like this:
  4   4   4   4
  5   5   5   4
  2   5   7   5
The player selects one of these tiles, and that tile will disappear, along with any tiles with the same number that are connected with that tile (up, down, left, or right), and any tiles with the same number connected with those, and so on. For example, if the 4 in the upper left corner is selected, these five tiles disappear
 >4< >4< >4< >4<
  5   5   5  >4<
  2   5   7   5
If the 5 just below it is selected, these four tiles disappear. Note that tiles are not connected diagonally.
  4   4   4   4
 >5< >5< >5<  4
  2  >5<  7   5
Write a function that, given a grid of tiles and a selected row and column of a tile, returns how many tiles will disappear.
grid1 = [[4, 4, 4, 4],
         [5, 5, 5, 4],
         [2, 5, 7, 5]]
disappear(grid1, 0, 0)  => 5
disappear(grid1, 1, 1)  => 4
disappear(grid1, 1, 0)  => 4
This is the grid from above.

Additional Inputs
grid2 = [[0, 3, 3, 3, 3, 3, 3],
         [0, 1, 1, 1, 1, 1, 3],
         [0, 2, 2, 0, 2, 1, 4],
         [0, 1, 2, 2, 2, 1, 3],
         [0, 1, 1, 1, 1, 1, 3],
         [0, 0, 0, 0, 0, 0, 0]]

grid3 = [[0]]

grid4 = [[1, 1, 1],
         [1, 1, 1],
         [1, 1, 1]]

All Test Cases
disappear(grid1, 0, 0)  => 5
disappear(grid1, 1, 1)  => 4
disappear(grid1, 1, 0)  => 4
disappear(grid2, 0, 0)  => 12
disappear(grid2, 3, 0)  => 12
disappear(grid2, 1, 1)  => 13
disappear(grid2, 2, 2)  => 6
disappear(grid2, 0, 3)  => 7
disappear(grid3, 0, 0)  => 1
disappear(grid4, 0, 0)  => 9

N - Width of the grid
M - Height of the grid
*/

public class Majong {


    final int[][] offsets = {{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
    int colMax;
    int rowMax;

    public static void main(String[] argv) {
        int[][] grid1 = {{4, 4, 4, 4},
                {5, 5, 5, 4},
                {2, 5, 7, 5}};
        int[][] grid2 = {{0, 3, 3, 3, 3, 3, 3},
                {0, 1, 1, 1, 1, 1, 3},
                {0, 2, 2, 0, 2, 1, 4},
                {0, 1, 2, 2, 2, 1, 3},
                {0, 1, 1, 1, 1, 1, 3},
                {0, 0, 0, 0, 0, 0, 0}};
        int[][] grid3 = {{0}};
        int[][] grid4 = {{1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}};


//        disappear(grid1, 0, 0)  => 5
//        disappear(grid1, 1, 1)  => 4
//        disappear(grid1, 1, 0)  => 4
//        disappear(grid2, 0, 0)  => 12
//        disappear(grid2, 3, 0)  => 12
//        disappear(grid2, 1, 1)  => 13
//        disappear(grid2, 2, 2)  => 6
//        disappear(grid2, 0, 3)  => 7
//        disappear(grid3, 0, 0)  => 1
//        disappear(grid4, 0, 0)  => 9

        Majong solution = new Majong();
//        System.out.println(solution.getCount(grid1, 0, 0));
        System.out.println(solution.getCount(grid1, 1, 1));
//        System.out.println(solution.getCount(grid1, 1, 0));
//        System.out.println(solution.getCount(grid2, 0, 0));
//        System.out.println(solution.getCount(grid2, 3, 0));
//        System.out.println(solution.getCount(grid2, 1, 1));
//        System.out.println(solution.getCount(grid2, 2, 2));
//        System.out.println(solution.getCount(grid2, 0, 3));
//        System.out.println(solution.getCount(grid3, 0, 0));
//        System.out.println(solution.getCount(grid4, 0, 0));
    }

    private int getCount(int[][] grid, int row, int col) {
        this.rowMax = grid.length;
        this.colMax = grid[0].length;

     return 0;

    }

//    int[][] grid1 = {{4, 4, 4, 4},
//            {5, 5, 5, 4},
//            {2, 5, 7, 5}};
    private int getCountHelper(int[][] grid, int numToCount, int row, int col) {

        //check out of bounds  4 > 4
        if (row < 0 || col < 0 || rowMax <= row || col >= colMax || grid[row][col] != numToCount) {
            return 0;
        }

        grid[row][col] = numToCount + 1;

        int count = 1;

        for (int[] offset : offsets) {
          count = count + getCountHelper(grid, numToCount, row + offset[0], col + offset[1]);   // returns 0 or the count + 1
        }

        return count;

    }


    private boolean isMahjongHand(String mahjongHand) {
        //create array count for each integer
        //iterate through array
        //does a pair exist
        //number of triplets
        int[] numCount = new int[10];

        for (int i = 0; i < mahjongHand.length(); i++) {
            int num = Character.getNumericValue(mahjongHand.charAt(i));
            numCount[num]++;
        }

        boolean doesPairExist = false;

        for (int i = 0; i < numCount.length; i++) {

            if (numCount[i] != 0) {

                int triplets = numCount[i] / 3;
                numCount[i] = numCount[i] - (triplets * 3);
                int pairs = numCount[i] / 2;
                if (pairs > 0) {
                    numCount[i] = numCount[i] - (pairs * 2);
                    if (doesPairExist) {
                        return false;
                    } else {
                        doesPairExist = true;
                    }
                }
                if (numCount[i] > 0) {
                    return false;
                }
            }
        }

        return doesPairExist;


    }
}
