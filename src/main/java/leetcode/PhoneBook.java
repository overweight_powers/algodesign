package leetcode;

import java.util.*;

public class PhoneBook {

    private final Map<Character, String> numpad = Map.of(
            '2', "abc", '3', "def", '4', "ghi", '5', "jkl",
            '6', "mno", '7', "pqrs", '8', "tuv", '9', "wxyz");

    private List<String> res = new ArrayList<>();

    public static void main(String[] args) {

        PhoneBook phoneBook = new PhoneBook();
        System.out.println(phoneBook.letterCombinations("23"));

    }


    public List<String> letterCombinations(String digits) {

        Deque<String> queue = new ArrayDeque<>();

        queue.offerFirst("");


        for (char digit : digits.toCharArray()) {  // '2' '3'
            int length = queue.size();
            for (int i = 0; i < length; i++) {
                String s = queue.pollFirst(); // 'a'
                for (char c : numpad.get(digit).toCharArray()) { //'d'e'f'
                    queue.addLast(s + c);
                }
            }
        }
        return queue.stream().toList();
    }



    public List<String> letterCombinationsRecursive(String digits) {

        if (digits == null || digits.isBlank()) {
            return res;
        }

        recursiveFunction(new StringBuilder(), 0, digits);

        return res;
    }

    private void recursiveFunction(StringBuilder sb, int index, String digits) {

        if (sb.length() >= digits.length()) {
            res.add(sb.toString());
            return;
        }


        String nums = numpad.get(digits.charAt(index));
        for (char c : nums.toCharArray()) {
            recursiveFunction(sb.append(c), index + 1, digits);
            sb.deleteCharAt(index);
        }

    }

}
