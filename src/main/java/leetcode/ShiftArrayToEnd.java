package leetcode;

import java.util.Arrays;

public class ShiftArrayToEnd {

    public static void main (String[] args){
        ShiftArrayToEnd solution = new ShiftArrayToEnd();
        int[] arrayToShift = new int[]{1,2,3,4,5};
        solution.shiftArray(arrayToShift, 4);
       // System.out.println("Result" + Arrays.toString(arrayToShift));
    }

    private void shiftArray(int[] arrayToShift, int shiftNum) {
        int[] solution = new int[arrayToShift.length];

        // 1 2 3 4 5 6 7
        int counter = 0;
        for(int i = 0; i < arrayToShift.length; i++){
            if(i < shiftNum){
                solution[i + arrayToShift.length - shiftNum] = arrayToShift[i];
            }
            else{
                solution[counter++] = arrayToShift[i];
            }
        }
        System.out.println("Result" + Arrays.toString(solution));
    }

}
