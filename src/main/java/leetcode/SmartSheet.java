package leetcode;

import java.util.*;

public class SmartSheet {

    public static void main(String[] args) {
        System.out.println("Hello, Py!");
        System.out.println("You're running Java!");

        Set<SheetLink> existingLinks = new HashSet<>();
        existingLinks.add(new SheetLink(0, 0, 0, 6)); // map x to y in above graph
        existingLinks.add(new SheetLink(0, 6, 2, 3)); // map y to z in above graph

        SheetLink newLink = new SheetLink(2, 3, 0, 1); // map z to x in above graph
        System.out.println(isCircularTopo(existingLinks, newLink));
    }


    record Node(int colId, int rowId) { }
    // links two cells in a grid(sheet)
    record SheetLink (
         int sourceRowId,
         int sourceColId,
         int destRowId,
         int destColId) {}

    // problem : is the given graph of sheet links circular
    // assume existing links do not form a circle
    // does adding this newlink form a circle        A -> C   D -> F  F -> A   A -> F
    // A - > B  -- not circular
    // is circular if existing link has data points of A && B existing  ?   new link with A - > B
    //TOPOLOGICAL SORT CHECK
    private static boolean isCircularTopo(Set<SheetLink> existingLinks, SheetLink newLink) {
        HashMap<Node, Deque<Node>> relationalMap = new HashMap<>();
        HashMap<Node, Integer> indegree = new HashMap<>();

        existingLinks.add(newLink);

        //Build Relational map  src -> dest
        for (SheetLink link : existingLinks) {
            Node destinationNode = new Node(link.destRowId, link.destColId);
            Node sourceNode = new Node(link.sourceRowId, link.sourceColId);

            //Build relational Mapp
            Deque<Node> relationList = relationalMap.getOrDefault(sourceNode, new ArrayDeque<>());
            relationList.add(destinationNode);
            relationalMap.put(sourceNode, relationList);

            //Build Indegree count map
            indegree.put(destinationNode, indegree.getOrDefault(destinationNode, 0) + 1);
        }

        Deque<Node> rootNodes = new ArrayDeque<>();
        for(Map.Entry<Node, Deque<Node>> entry: relationalMap.entrySet()){
            if(!indegree.containsKey(entry.getKey())){
                rootNodes.addLast(entry.getKey());
            }
        }

        int removedEdges = 0;
        while (!rootNodes.isEmpty()) {
            Node root = rootNodes.poll();
            if(relationalMap.containsKey(root)){
                Deque<Node> destinationList = relationalMap.get(root); //getting list of nodes that root is targeting
                for(Node destinationNode : destinationList){
                        indegree.put(destinationNode, indegree.get(destinationNode)-1);  //subtract indegree by 1 (effectively deleting the relationship)
                        removedEdges++;
                        if(indegree.get(destinationNode) == 0){  //found another root node with no target dependencies
                            rootNodes.add(destinationNode);

                        }
                }
            }
        }


        return removedEdges == existingLinks.size();
    }



    /*
    Pseudocode for above:
    Create Adjacency hashmap list of (src -> target)  and indegree hashmap count. So if a node x has 2 other nodes pointing to it, that would be {x, 2}
    Iterate through the adjacency list keys checking the indegre map. Anything with NO indegree IE no existing in the indegree map, is a ROOT
    Append the root nodes to a queue

    counter for edges removed
    while queue is not empty {
        pop root node
        get the adjacent nodes for the root node out of the hashmap
        iterate through adjacent nodes ->
            subtract an indegree for each node -> if resulting indegree is 0 than add to queue
            add 1 to the counter of edges removed. This process you are effectively removing each edge by the indegree count
    }
    return edges removed == size of edges list
     */




    private static boolean isCircularBackTracking(Set<SheetLink> existingLinks, SheetLink newLink) {

        HashMap<Node, Deque<Node>> relationalMap = new HashMap<>();


        //Build Relational map  src -> dest
        for (SheetLink link : existingLinks) {
            Node destinationNode = new Node(link.destRowId, link.destColId);
            Node sourceNode = new Node(link.sourceRowId, link.sourceColId);

            //Build relational Mapp
            Deque<Node> relationList = relationalMap.getOrDefault(sourceNode, new ArrayDeque<>());
            relationList.add(destinationNode);
            relationalMap.put(sourceNode, relationList);
        }


        Node sourceNode = new Node(newLink.sourceRowId, newLink.sourceColId);
        Deque <Node> neighbors = relationalMap.get(sourceNode);
        Deque <Node> neighborList = new LinkedList<>();
        neighborList.addAll(neighbors);

        while (!neighborList.isEmpty()) {
            Node curr = neighborList.poll();
            Deque<Node> currNeighbors = relationalMap.getOrDefault(curr, new ArrayDeque<>());
            for (Node neighbor : currNeighbors) {
                neighborList.addAll(currNeighbors);
                if (neighbor.rowId == newLink.destRowId && neighbor.colId == newLink.destColId)
                    return true;
            }
        }

        return false;
    }
}
