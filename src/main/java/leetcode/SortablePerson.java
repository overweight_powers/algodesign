package leetcode;

public class SortablePerson {

    private Integer age;

    public SortablePerson(int age){
        this.age = age;
    }

    public Integer getAge(){
        return age;
    }
}
