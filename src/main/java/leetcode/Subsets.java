package leetcode;

import java.util.*;

class Subsets {

    private List<List<Integer>> res = new ArrayList<>();
    private int[] nums;
    private int subsetSize;

    public static void main(String[] args) {
        Subsets subsets = new Subsets();
        System.out.println(subsets.subsets(new int[]{2,3,4,5,6}).toString());
    }



    public List<List<Integer>> subsets(int[] nums) {

        this.nums = nums;

        if (nums == null || nums.length == 0) {
            return res;
        }

        for (this.subsetSize = 1; this.subsetSize <= nums.length; this.subsetSize++) {
            addUniqueSubsets(new LinkedList<Integer>(), 0);
        }


        return res;

    }

    private void addUniqueSubsets(LinkedList<Integer> subset, int index) {

        if (subset.size() >= this.subsetSize) {
            res.add(new ArrayList<>(subset));
            return;
        }
  //1  2  e
        for (int i = index; i < this.nums.length; i++) {
            subset.offerLast(this.nums[i]);
            addUniqueSubsets(subset, i + 1);
            //backtracking
            subset.removeLast();
        } //123


    }




//        public void backtrack(int first, ArrayList<Integer> curr, int[] nums) {
//            // if the combination is done
//            if (curr.size() == k) {
//                output.add(new ArrayList(curr));
//                return;
//            }
//            for (int i = first; i < n; ++i) {
//                // add i into the current combination
//                curr.add(nums[i]);
//                // use next integers to complete the combination
//                backtrack(i + 1, curr, nums);
//                // backtrack
//                curr.remove(curr.size() - 1);
//            }
//        }


}
