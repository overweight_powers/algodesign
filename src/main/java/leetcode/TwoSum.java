package leetcode;

import java.util.*;

public class TwoSum {

    /*
    Given an int array nums and an int target,
    find how many unique pairs in the array such that their sum is equal to target.
    Return the number of pairs.

    Input: nums = [1, 1, 2, 45, 46, 46], target = 47
    Output: 2
    Explanation:
    1 + 46 = 47
    2 + 45 = 47
     */

    public static void main(String[] args) {
        int[] nums = {1, 46, 1, 1, 1,  2, 45, 46, 46};

        int target = 47;
        TwoSum twoSum = new TwoSum();
        System.out.println(twoSum.uniquePairs(nums, target));
    }

    public int uniquePairs(int[] nums, int target){
       int count = 0;
       if(nums == null || nums.length == 0){
           return count;
       }

       HashSet<Integer> pair = new HashSet<>();
       HashSet<Integer> seen = new HashSet<>();
       for(int num : nums){
          if(seen.contains(target - num) && !pair.contains(num)){
              pair.add(num);
              pair.add(target - num);
              count++;
          }
           seen.add(num);
       }
        return count;
    }

}
