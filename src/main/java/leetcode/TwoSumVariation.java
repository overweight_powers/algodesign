package leetcode;

import java.util.Arrays;

public class TwoSumVariation {

    public static void main(String args[]){
        TwoSumVariation sol = new TwoSumVariation();
        int[] input = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int target = 10;
        System.out.println(Arrays.toString(sol.getClosestPair(input, target)));
    }

    private int[] getClosestPair(int[] nums, int target){
        int[] res = new int[2];
        Arrays.sort(nums);

        int minDiff = Integer.MAX_VALUE;
        for (int lo = 0, hi = nums.length - 1; lo < hi; ) {
            int sum = nums[lo] + nums[hi];
            int diff = Math.abs(target - sum);
            if (diff < minDiff) {
                minDiff = diff;
                res[0] = nums[lo];
                res[1] = nums[hi];
            }
            if (sum < target) {
                lo++;
            } else if (sum > target) {
                hi--;
            } else {
                break;
            }
        }
        return res;
    }
}
