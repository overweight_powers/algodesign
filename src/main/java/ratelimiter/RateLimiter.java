package ratelimiter;

public interface RateLimiter {
    boolean isValid(String userId);
}

/*
QUESTION:  specific requirements that it had to limit based on time and reject it if it exceeded n requests within a x time window


Things to watch out for when implementing
*Off by one error.
    - Since we are keeping a counter of ts requests, we need to make sure we are keeping the logic tightly coupled for
     appending the counter and accepting the request.
        Example: If we append counter, but than deny request, no request can get through in a steady stream.
                Or we don't append counter and allow request, all requests will end up getting through despite overflow.


Sliding window
* Easier to implement
* Faster as you don't need to iterate over the entire list, just filter and check the size
* Less memory efficient. List will reach size of O(rateLimitSize)

Sliding window counter:
* More complex to implement
* More memory efficient
* Need to iterate over a users entire request list for every request to determine the count and find the timestamp/count to append
 */
