package ratelimiter;

import lombok.Getter;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Getter
public class RequestCounter {

        private final Instant timestamp;
        private int count;

        public RequestCounter(){
            this.count = 1;
            this.timestamp = Instant.now().truncatedTo(ChronoUnit.SECONDS);
        }

        public void hit(){
            this.count++;
        }
}
