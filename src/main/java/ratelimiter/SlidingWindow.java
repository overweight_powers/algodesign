package ratelimiter;


import lombok.Data;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
//Sliding Window Log
public class SlidingWindow implements RateLimiter {

    private Map<String, LinkedList<Instant>> requestLog = new ConcurrentHashMap<>();
    private int rateLimit;
    private Duration timeWindow;


    public SlidingWindow(int rate, long seconds) {
        this.rateLimit = rate;
        timeWindow = Duration.ofSeconds(seconds);
        System.out.println("RateLimiterSliding initialized with rate limit of: " + rateLimit + " in a time window of: " + timeWindow + " seconds");
    }


    public synchronized boolean isValid(String userId) {
        //New user request
        if (!requestLog.containsKey(userId)) {
            System.out.println("adding new user: " + userId);
            addNewUser(userId);
            return true;
        }

        //User is not new
        else {
            Instant now = Instant.now();
            Instant beginningOfWindow = now.minus(timeWindow);
            LinkedList<Instant> requests = requestLog.get(userId);

            //User has not made any requests in the most recent window of time
            if (!requests.isEmpty() && requests.peekLast().isBefore(beginningOfWindow)) {
                System.out.println("User has not made any requests in the most recent window of time, clearing list of requests: " + userId);
                requests.clear();
                requests.offerLast(now);
                return true;
            }

            //User has requests less than limit
            if (requests.isEmpty() || requests.size() < this.rateLimit) {
                System.out.println("User has requests less than limit: " + userId);
                requests.offerLast(now);
                return true;
            }

            //Most common scenario, filter old requests and get count of requests in time window
            else {
                while (!requests.isEmpty() && requests.peekFirst().isBefore(beginningOfWindow)) {
                    System.out.println("Filtering old requests for user: " + userId);
                    requests.pollFirst();
                }

                if (requests.size() > this.rateLimit) {
                    return false;
                } else {
                    requests.offerLast(now);
                    return true;
                }
            }
        }
    }

    private void addNewUser(String userId) {
        LinkedList<Instant> requests = new LinkedList<>();
        requests.offerLast(Instant.now());
        requestLog.put(userId, requests);
    }


}
