//package ratelimiter;
//
//
//import lombok.Data;
//
//import java.time.Duration;
//import java.time.Instant;
//import java.time.temporal.ChronoUnit;
//import java.util.LinkedList;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//@Data
////Sliding Window Counter
//public class SlidingWindowCounter implements RateLimiter {
//
//    private Map<String, LinkedList<RequestCounter>> requestLog = new ConcurrentHashMap<>();
//    private int rateLimit;
//    private Duration timeWindow;
//
//
//    public SlidingWindowCounter(int rate, long seconds) {
//        this.rateLimit = rate;
//        timeWindow = Duration.ofSeconds(seconds);
//        System.out.println("RateLimiterSlidingCounter initialized with rate limit of: " + rateLimit + " in a time window of: " + timeWindow + " seconds");
//    }
//
//    public synchronized boolean isValid(String userId) {
//
//        //New user request
//        if (!requestLog.containsKey(userId)) {
//            System.out.println("adding new user: " + userId);
//            addNewUser(userId);
//            return true;
//        }
//
//        //User is not new
//        else {
//            Instant now = Instant.now();
//            Instant beginningOfWindow = now.minus(timeWindow);
//            LinkedList<RequestCounter> requests = requestLog.get(userId);
//
//            //User has not made any requests in the most recent window of time
//           // (now().isAfter(expiration))
//            if (!requests.isEmpty() && requests.peekLast().getTimestamp().isBefore(beginningOfWindow)) {
//                System.out.println("User has not made any requests in the most recent window of time: " + userId);
//                requests.clear();
//                requests.offerLast(new RequestCounter());
//                return true;
//            }
//
//            //Most common scenario, filter old requests and get count of requests in time window
//            else {
//                filterOldRequests(requests, beginningOfWindow, userId);
//                return isRequestUnderLimit(userId, Instant.now().truncatedTo(ChronoUnit.SECONDS));
//            }
//        }
//    }
//
//
//    private void addNewUser(String userId) {
//        LinkedList<RequestCounter> requests = new LinkedList<>();
//        requests.offerLast(new RequestCounter());
//        requestLog.put(userId, requests);
//    }
//
//    private void filterOldRequests(LinkedList<RequestCounter> requests, Instant beginningOfWindow, String userId) {
//        while (!requests.isEmpty() &&
//                requests.peekFirst().getTimestamp().isBefore(beginningOfWindow)) {
//            System.out.println("Filtering old requests for user: " + userId);
//            requests.pollFirst();
//        }
//    }
//
//
//    //Iterate through users requests, getting count of requests and appending count if found and below limit
//    private synchronized boolean isRequestUnderLimit(String userID, Instant ts) {
//        LinkedList<RequestCounter> requests = this.getRequestLog().getOrDefault(userID, new LinkedList<>());
//        Long count = 0L;
//        RequestCounter foundReq = null;
//        for (RequestCounter req : requests) {
//            if (req.getTimestamp().equals(ts)) {
//                System.out.println("Timestamp match ts found");
//                foundReq = req;
//            }
//            count = count + req.getCount();
//        }
//
//        //If we are below rate limit, then we can add the request to the log and allow it through
//        if (count < this.rateLimit) {
//            if (foundReq == null) {
//                requests.offerLast(new RequestCounter());
//            } else {
//                foundReq.hit();
//            }
//            return true;
//        }
//        return false;
//    }
//
//}
