//package ratelimiter.tokenbucket;
//
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.atomic.AtomicInteger;
//
//interface RateLimiter {
//
//    /**
//     * Perform rate limiting logic for provided customer ID. .
//     *
//     * @param customerId
//     * @return Return true if the request is allowed, and false if it is not
//     */
//    boolean rateLimit(int customerId);
//
//}
//
//abstract class AbstractRateLimiter implements RateLimiter {
//
//    protected final int maxRequests;
//
//    public AbstractRateLimiter(int maxRequests) {
//        this.maxRequests = maxRequests;
//    }
//
//    @Override
//    public abstract boolean rateLimit(int customerId);
//}
//
//class TokenBucketRateLimiter extends AbstractRateLimiter {
//
//    private final int rateLimitWindow;
//    private AtomicInteger tokenBucketCount;
//
//    public TokenBucketRateLimiter(int maxRequests, int rateLimitWindow) {
//        super(maxRequests);
//        this.rateLimitWindow = rateLimitWindow;
//        this.tokenBucketCount = new AtomicInteger(0);
//
//        new Thread(() -> {
//            while (true) {
//                try {
//                    TimeUnit.SECONDS.sleep(1);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                refillBucket(maxRequests);
//            }
//        }).start();
//    }
//
//    @Override
//    public boolean rateLimit(int customerId) {
//
//        System.out.println("Current Token Count: " + tokenBucketCount.get());
//        tokenBucketCount.set(tokenBucketCount.incrementAndGet());
//        return tokenBucketCount.get() <= maxRequests;
//    }
//
//    private void refillBucket(int maxRequests) {
//        synchronized (this) {
//            this.tokenBucketCount.set(0);
//            notifyAll();
//        }
//    }
//}
//
//class Customer {
//
//    private int id;
//
//    private String name;
//
//    public Customer(int id, String name) {
//        this.id = id;
//        this.name = name;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    @Override
//    public int hashCode() {
//        return this.id;
//    }
//}
//
//public class RateLimiterService {
//
//    Map<Customer, RateLimiter> customerLimitMap;
//    private final int maxRateLimit;
//    private final int rateLimitWindow;
//
//    public static final int MAX_REQUESTS_PER_CUSTOMER = 10;
//    public static final int RATE_LIMIT_WINDOW_IN_SECONDS = 1;
//
//    public RateLimiterService(int maxRateLimit, int rateLimitWindow) {
//        this.maxRateLimit = maxRateLimit;
//        this.rateLimitWindow = rateLimitWindow;
//        this.customerLimitMap = new ConcurrentHashMap<>();
//    }
//
//    public void accessApplication(Customer customer) {
//
//        RateLimiter rateLimiter;
//        if (customerLimitMap.containsKey(customer)) {
//            rateLimiter = customerLimitMap.get(customer);
//        } else {
//            rateLimiter = new TokenBucketRateLimiter(maxRateLimit, rateLimitWindow);
//            customerLimitMap.put(customer, rateLimiter);
//        }
//
//        if (rateLimiter.rateLimit(customer.getId())) {
//            System.out.printf("Customer=%s : HTTP 200%n", customer.getName());
//        } else {
//            System.out.printf("Customer=%s : HTTP 429 Rate Limit Exceeded%n", customer.getName());
//        }
//
//    }
//
//    public static void main(String[] args) {
//
//        Customer customer1 = new Customer(1, "John");
//        RateLimiterService service = new RateLimiterService(MAX_REQUESTS_PER_CUSTOMER, RATE_LIMIT_WINDOW_IN_SECONDS);
//
////        ExecutorService executorService = Executors.newFixedThreadPool(15);
////        for(int i=0; i<15; i++){
////            executorService.execute(() -> service.accessApplication(customer1));
////        }
//
//        for (int i = 0; i < 15; i++) {
//            service.accessApplication(customer1);
//        }
//
////        executorService.shutdown();
//    }
//}
